import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import Home from './home';
// import WeatherApi from './Component/WeatherApi';
import NotFound from './Component/NotFound';

function App() {

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Home></Home>,
    },
    {
      path: "/*",
      element: <NotFound/>,
    },
    // {
    //   path:"/weather",
    //   element: <WeatherApi/>
    // }

  ]);




  return (
    <div className="App">

      <RouterProvider router={router} ></RouterProvider>

    </div>
  );
}

export default App;
