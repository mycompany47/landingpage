import styled from "styled-components";


export const MainContainer = styled.div`
    width: 100%;
    margin-top: 50px;
    background: #000
    `
;

export const Container = styled.div`
    display: flex;
    width: 66.67%;
    background: black;
    padding-top: 70px;
    padding-bottom: 70px;
    margin: auto;
    display: flex;
    justify-content: center;
    align-items: center;`
    ;

export const Card = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr); 
    grid-gap: 20px; 
    width:100%;
    @media (max-width: 768px) {
        width: unset;   
        grid-template-columns: repeat(2, 4fr); 
 
       }  
`;

export const Box = styled.div`
    font-size: 16px;
    line-heigt: 24px;
    color: #fff;
    width: 396px;
    font-weight: 400;
    text-align:left;
    font-family: 'Plus Jakarta Sans', sans-serif;  
    @media (max-width: 768px) {
       width: unset;    
      }  
    `;



export const BoxHeading = styled.div`
    font-size: 16px;
    color: #fff;
    line-heigt: 24px;
    text-align:left;
    font-weight: 400;
    font-family: 'Plus Jakarta Sans', sans-serif; 
`;


export const BoxList = styled.div`
    font-size: 16px;
    line-heigt: 24px;
    color: #fff;
    text-align:center;
    font-weight: 400;
    font-family: 'Plus Jakarta Sans', sans-serif; 
`;



export const ListsHeading = styled.ul`
    font-size: 16px;
    list-style: none;
    line-heigt: 24px;
    text-align:center;
    font-weight: 400;
    font-family: 'Plus Jakarta Sans', sans-serif; 
    @media (max-width: 768px) {
       }  
`;



export const Lists = styled.ul`
    font-size: 16px;
    list-style: none;
    line-height: 40px;
    text-align:center;
    font-weight: 400;
    font-family: 'Plus Jakarta Sans', sans-serif; 
`;


export const SocialImage = styled.img`
 
`;


export const SocialBox = styled.div`
    margin-top: 50px;
`;


export const Content = styled.div`
    margin-top: 50px;
    font-family: 'Plus Jakarta Sans', sans-serif; 
    line-height: 22px;
    font-size: 14px;
    color:rgba(255, 255, 255, 1);
    @media (max-width: 768px) {
        margin-top: 10px;
    }  
`













