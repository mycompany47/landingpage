import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    width: 66.67%;
    margin: auto;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width: 768px) {
        width: unset;   
    }
    
    `;

export const ExploreHeading = styled.div`
    font-family: 'Plus Jakarta Sans', sans-serif;
    font-weight: 700;
    font-size: 40px;
    line-height: 60px;
    margin-top: 40px;
    color: #0F0049;
    @media (max-width: 768px) {
        font-size: 24px;
    }
`
    ;

export const ExploreParagraph = styled.div`
    font-size: 18px;
    font-family: 'Plus Jakarta Sans', sans-serif;
    line-height: 28px;
    font-weight: 400;
    @media (max-width: 768px) {
        width: unset;
        font-size: 16px;
        text-align:center;
   
    }
    
`
export const Cards = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr); 
    width: 90%;
    margin: 40px auto;
    @media (max-width: 768px) {
        width: unset;
        margin 10px auto;
        display: block;

    }
`

export const Cards1 = styled.div`
    display: flex;
    flex-direction: column; 
    max-width: 200px; 
    @media (max-width: 768px) {
      max-width:unset;
      margin: 10px;
    }

`

export const Image = styled.img`
    width: 15%;
    padding-left: 10px;
`;


export const CardHead = styled.div`
    font-size: 20px;
    font-family: 'Plus Jakarta Sans', sans-serif;
    text-align: left;
    font-weight: 500;
    line-height: 25.2px;
    color: #0F0049;
    padding-left: 10px;
    margin-top: 20px;
`;

export const CardsBox = styled.div`
    background: red;    
`;

export const CardPara = styled.div`

    font-size: 16px;
    font-family: 'Plus Jakarta Sans', sans-serif;
    text-align: left;
    font-weight: 400;
    line-height: 20.16px;
    color: rgba(15, 0, 73, 1);
    padding-left: 10px;
    width: 99%;
    margin-top: 20px;
`

export const StyledButton = styled.button`
  background-color: #6153cd;
  color: white;
  border: none;
  font-size:16px;
  line-height:24px;
  text-align:center;
  padding: 7px 24px 7px 24px;
  font-family: 'Plus Jakarta Sans', sans-serif;
  border-radius: 8px;
  cursor: pointer;
`;




