import styled from "styled-components";


export const Container = styled.div`
  display: flex;
  width: 66.67%;
  margin: 110px auto; 
  @media (max-width: 768px) {
    display: block;
    width: 100%;
    margin: 20px auto; 
}  
`;

export const ContainerHeading = styled.div`
  display: flex;
  width: 66.67%;
  margin: 0px auto; 
  
`;

export const SpanText = styled.span`
    
    color: rgba(97, 83, 205, 1);
  
`;






export const RightSection = styled.div`
    width:50%;
    @media (max-width: 768px) {
        width: 100%;
        text-align:center;
        margin-top:20px;
    }

`

export const LeftSection = styled.div`
    width:50%;
    text-align: left;
    @media (max-width: 768px) {
        width: 100%;
        text-align: center;

      }
`

export const Heading = styled.div`
    font-size: 60px;
    font-family: 'Plus Jakarta Sans', sans-serif;
    line-height: 75.6px;
    font-weight: 700;
    width: 81%;
    text-align: left;
    @media (max-width: 768px) {
        width: unset;
        font-size: 25px;
        line-height: 30px;
        text-align:center;
    } 

`

export const Paragraph = styled.div`
    font-size: 18px;
    font-family: 'Plus Jakarta Sans', sans-serif;
    line-height: 28px;
    text-align: left;
    width:82%;
    margin-top:10px;
    font-weight: 500;
    @media (max-width: 768px) {
        width: unset;
        text-align:center;
    } 
`
export const span = styled.div`
    background-color: red;
`

export const Image = styled.img`
    @media (max-width: 768px) {
        width: 60%;
    } 
`;

export const StyledButton = styled.button`
  background-color: #6153cd;
  color: white;
  border: none;
  font-size:16px;
  line-height:24px;
  padding: 7px 24px 7px 24px;
  font-family: 'Plus Jakarta Sans', sans-serif;
  border-radius: 8px;
  cursor: pointer;
  text-align: left;
  margin-top: 20px;

`;


export const clientBox = styled.div`
    align-items:center;
    display:flex;
    @media (max-width: 768px) {
      justify-content:center;
      margin: 20px auto;
      width: 60%;
    } 
`
export const Client = styled.div`
    display: grid;
    grid-template-columns: repeat(5, 1fr); 
    width: 80%;
    margin:auto;
    @media (max-width: 768px) {
        display: block;

    } 
`
export const ClientHeading = styled.div`
    margin:auto;
`

export const TextClient = styled.div`
    text-align: center;
    font-size: 14px;
    font-family: 'Plus Jakarta Sans', sans-serif;
    font-weight: 400;
    line-height: 17.64px;

`


export const ClientSection = styled.div`
    margin-top: 100px;
    @media (max-width: 768px) {
        margin-top: unset;

    } 
`







