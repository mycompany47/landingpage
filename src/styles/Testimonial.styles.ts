import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    width: 66.67%;
    margin: auto;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width: 768px) {
        width: unset; 
      }
    `
    ;

export const TestimonialSection = styled.div`
    width: 95%;
    margin: 70px auto;
    display: grid;
    grid-template-columns: repeat(2, 1fr); 
    @media (max-width: 768px) {
        grid-template-columns: repeat(1, 1fr); 
      }

    `
;

export const TestimonialImage = styled.div`
    display:flex;
    width:70%
    @media (max-width: 768px) {
       width: unset;
      }
    `
    
;

export const Image = styled.img`

@media (max-width: 768px) {
    margin: auto;
  }
    `
;

export const TestimonialText = styled.div`
    text-align: initial;
    font-size: 24px;
    font-family: 'Plus Jakarta Sans', sans-serif;
    font-weight: 500;
    color: rgba(71, 83, 107, 1);
    width:756px;
    @media (max-width: 768px) {
        width: unset; 
        margin-top: 10px;
        font-size: 19px;
        text-align:center;

      }
    `
;

export const TestimonialTextBox = styled.div`
   

`
;

export const TestimonialTextPara = styled.div`
    text-align: initial;
    font-family: 'Plus Jakarta Sans', sans-serif;
    margin-top: 30px;
    font-size: 20px;
    color: rgba(71, 83, 107, 1);
    @media (max-width: 768px) {
        text-align:center;

      }

    `
;

export const TestimonialArrow = styled.div`
    justify-content: right;
    display: flex;    
    margin-top: 250px; 
    @media (max-width: 768px) {
        justify-content: center;
        display: flex;
        margin-top: 20px;
      }
    `
;










