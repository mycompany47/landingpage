import styled from "styled-components";


export const Container = styled.div`
    display: flex;
    width: 66.67%;
    margin: auto;
    justify-content: left;
    align-items: center;
    @media (max-width: 768px) {
        width: unset; 
        display: block;   
      }
    `
;

export const ContainerButton = styled.div`
    margin-top: 160px;  
    text-align: center;
        `
;



export const BlogHeading = styled.div`
    font-family: 'Plus Jakarta Sans', sans-serif;
    font-weight: 700;
    text-align: left;
    font-size: 40px;
    line-height: 60px;
    color: rgba(15, 0, 73, 1);
    @media (max-width: 768px) {
       text-align: center; 
      }

`;

export const Cards = styled.div`
   display: grid;
   margin-top: 50px;
   grid-template-columns: repeat(3, 1fr); 
   grid-gap: 20px;  
   @media (max-width: 768px) {
    grid-template-columns: repeat(1, 1fr); 
    margin-top: unset;


  }
`;

export const ImageBox = styled.div`
    text-align: -webkit-right;
    position: relative;
    @media (max-width: 768px) {
       position: unset;
    
    
      }
`;



export const Image = styled.img`
`;

export const ContentBox = styled.div`
   background: #fff;
   width: 272px;
   font-family: 'Plus Jakarta Sans', sans-serif;
   height: 233px;
   text-align: start;
   font-size:14px;
   padding: 20px;
   margin-top: -116px;
   line-height:20px;
   color:rgba(15, 0, 73, 1);
   position: absolute;
   right:0;
`;


export const Heading = styled.h2`
  font-size: 24px;
  line-height: 30.24px;
  color: rgba(15, 0, 73, 1); 
`;


export const ImageDiv = styled.div`
    text-align: -webkit-right;
`;


export const StyledButton = styled.button`
  background-color: #6153cd;
  color: white;
  border: none;
  font-size:16px;
  line-height:24px;
  text-align:center;
  padding: 7px 24px 7px 24px;
  font-family: 'Plus Jakarta Sans', sans-serif;
  border-radius: 8px;
  cursor: pointer;
`;

