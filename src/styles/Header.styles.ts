import styled from "styled-components";


export const HeaderContainer = styled.div`
  display: flex;
  width: 66.67%;
  margin: auto;  
  @media (max-width: 768px) {
    width: unset;
    display:block;

  }
`;

export const LeftSection = styled.div`
  width: 50%;
  text-align: left;
  justify-content: start;
  display: flex;
  align-items: center;
  @media (max-width: 768px) {
    width: unset;
    text-align:center;

  }
  
  `;

export const RightSection = styled.div`
  width: 50%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  @media (max-width: 768px) {
    width: unset;

  }
`;

export const SmallSection = styled.div`
  width: 20%;
  padding: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 768px) {
    width: unset;

  }
`;

export const StyledButton = styled.button`
  background-color: #6153cd;
  color: white;
  border: none;
  font-size: 14px;
  line-height:24px;
  text-align:center;
  padding: 7px 13px 7px 13px;
  font-family: 'Plus Jakarta Sans', sans-serif;
  border-radius: 8px;
  cursor: pointer;
`;

export const HeaderText = styled.text`
  color: #2D3748;
  border: none;
  font-family: 'Plus Jakarta Sans', sans-serif;
  line-height:20.16px;
  fontWeight: 400;
  cursor: pointer;
`

export const CompanyText = styled.text`
  font-family: 'Plus Jakarta Sans', sans-serif;
  font-size: 24px;
  font-weight: 700;
  line-height: 24px;
  letter-spacing: 0px;
  text-align: left;
`

export const Image = styled.img`
  width:21px;
  height:31px;
  padding:10px;
`;
