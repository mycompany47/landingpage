
import HeaderLists from './Component/ControlledHeader';
import ControlledFirstSection from "../src/Component/ControlledFirstSection";
import ControlledServices from "../src/Component/ControlledServices";
import ControlledTestimonialSection from './Component/ControlledTestimonial';
import ControlledBlog from './Component/ControlledBlogSection';
import FooterControlled from './Component/ControlledFooter';

function Home() {

  return (
    <div>
      <HeaderLists></HeaderLists>
      <ControlledFirstSection></ControlledFirstSection>
      <ControlledServices></ControlledServices>
      <ControlledTestimonialSection></ControlledTestimonialSection>
      <ControlledBlog></ControlledBlog>
      <FooterControlled></FooterControlled>
      
    </div>
  );
}

export default Home;
