import React from "react";
import * as styled from "../styles/FirstSection.styles"




interface FirstSectionProps {
    children?: React.ReactNode;
    Heading: string;
    BrandImage: string;
    paragraph: string;
    spanText: string;
    Button: string;
    Logo1: string;
    Logo2: string;
    Logo3: string;
    Logo4: string;
    Logo5: string;
    TextClient: string;

}


const FirstSection: React.FC<FirstSectionProps> = ({children, Logo1, TextClient, Logo2, Logo3, Logo4, Logo5, spanText, Heading, paragraph, Button, BrandImage }) => {

    return (

        <>

            <styled.Container>
                <styled.LeftSection>
                    <styled.Heading>
                        {Heading} <styled.SpanText>{spanText}</styled.SpanText>
                    </styled.Heading>
                    <styled.Paragraph>
                        {paragraph}
                    </styled.Paragraph>
                    <styled.StyledButton>{Button}</styled.StyledButton>
                </styled.LeftSection>
                <styled.RightSection>
                    <styled.Image src={BrandImage}></styled.Image>
                </styled.RightSection>

            </styled.Container>

            <styled.ClientSection>
                <styled.ContainerHeading>
                    <styled.ClientHeading>
                        <styled.TextClient>{TextClient}</styled.TextClient>
                    </styled.ClientHeading>
                </styled.ContainerHeading>
            </styled.ClientSection>



            <styled.Container>
                <styled.Client>
                    <styled.clientBox>
                        <styled.Image src={Logo1}></styled.Image>
                    </styled.clientBox>
                    <styled.clientBox>
                        <styled.Image src={Logo2}></styled.Image>
                    </styled.clientBox>
                    <styled.clientBox>
                        <styled.Image src={Logo3}></styled.Image>
                    </styled.clientBox>
                    <styled.clientBox>
                        <styled.Image src={Logo4}></styled.Image>
                    </styled.clientBox>
                    <styled.clientBox>
                        <styled.Image src={Logo5}></styled.Image>
                    </styled.clientBox>
                </styled.Client>

            </styled.Container>

        </>


    );
}

export default FirstSection