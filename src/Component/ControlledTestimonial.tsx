import TestimonialSection from "./TestimonialSection";

import TestimonialImage from "../assets/TestimonialAssets/Rectangle 21.png"

export default function ControlledTestimonialSection() {
    return (
        <TestimonialSection TestimonialPara={"David Calathan - Director of Design Operations, New York"} TestimonialText={"“The results have been incredible. With Power Digital, it feels like they’re in our trench, supporting and understanding us. They’re like a partner and mentor in helping us get where we want to be.”"} TestimonialImage={TestimonialImage} Arrow={"sfgd"}></TestimonialSection>
    )
}
