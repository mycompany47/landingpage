import React from 'react';
import { render } from '@testing-library/react';
import FirstSection from './FirstSection';

describe('FirstSection Component', () => {
  const mockProps = {
    Heading: 'Test Heading',
    BrandImage: 'brand.jpg',
    paragraph: 'Test paragraph',
    spanText: 'Test span',
    Button: 'Test button',
    Logo1: 'logo1.jpg',
    Logo2: 'logo2.jpg',
    Logo3: 'logo3.jpg',
    Logo4: 'logo4.jpg',
    Logo5: 'logo5.jpg',
    TextClient: 'Test Client Text',
  };

  test('renders FirstSection component with provided props', () => {
    const { getByText, getByAltText } = render(<FirstSection {...mockProps} />);

    expect(getByText('Test Heading Test span')).toBeInTheDocument();
    expect(getByText('Test paragraph')).toBeInTheDocument();
    expect(getByText('Test button')).toBeInTheDocument();

    expect(getByAltText('brand.jpg')).toBeInTheDocument();
    expect(getByAltText('logo1.jpg')).toBeInTheDocument();
    expect(getByAltText('logo2.jpg')).toBeInTheDocument();
    expect(getByText('Test Client Text')).toBeInTheDocument();
  });

});
