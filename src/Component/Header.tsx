import React from "react";
import * as styled from "../styles/Header.styles"



interface HeaderProps {
  Button: string;
  aboutText: string;
  servicesText: string;
  projectsText: string;
  CompanyName: string;
  CompanyLogo: string;
}


const Header: React.FC<HeaderProps> = ({ CompanyLogo, CompanyName, projectsText, aboutText, servicesText, Button }) => {

  return (
    <styled.HeaderContainer>
      <styled.LeftSection>
        <styled.Image src={CompanyLogo}></styled.Image>
        <styled.CompanyText>{CompanyName}</styled.CompanyText>
      </styled.LeftSection>
      <styled.RightSection>
          <styled.SmallSection>
            <styled.HeaderText>{aboutText}</styled.HeaderText>
          </styled.SmallSection>
          <styled.SmallSection>
            <styled.HeaderText>{servicesText}</styled.HeaderText>
          </styled.SmallSection>
          <styled.SmallSection>
            <styled.HeaderText>{projectsText}</styled.HeaderText>
          </styled.SmallSection>
          <styled.SmallSection>
            <styled.StyledButton>{Button}</styled.StyledButton>
          </styled.SmallSection>
      </styled.RightSection>
    </styled.HeaderContainer>

  );
}

export default Header