import React from "react";
import * as styled from "../styles/Testimonial.styles"




interface TestimonialSectionProps {
    children?: React.ReactNode;
    TestimonialText: string;
    TestimonialImage: string;
    TestimonialPara:string;
    Arrow: string;

}


const TestimonialSection: React.FC<TestimonialSectionProps> = ({ children, TestimonialText, TestimonialImage, TestimonialPara, Arrow }) => {

    return (

        <>

            <styled.Container>
                <styled.TestimonialSection>
                    <styled.TestimonialImage>
                        <styled.Image src={TestimonialImage}></styled.Image>
                    </styled.TestimonialImage>
                    <styled.TestimonialTextBox>
                        <styled.TestimonialText>{TestimonialText}</styled.TestimonialText>
                        <styled.TestimonialTextPara>{TestimonialPara}</styled.TestimonialTextPara>
                        <styled.TestimonialArrow>{Arrow}</styled.TestimonialArrow>
                    </styled.TestimonialTextBox>
                </styled.TestimonialSection>
            </styled.Container>

        </>


    );
}

export default TestimonialSection