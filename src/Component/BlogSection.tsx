import React from "react";
import * as styled from "../styles/Blog.styles"


interface BlogProps {
    children?: React.ReactNode;
    BlogHead: string;
    Image1: string;
    Image2: string;
    Image3: string;
    content1: string;
    HeadingContent: string;
    Arrow: string;
    Button: string;



};
const Blog: React.FC<BlogProps> = ({ children, BlogHead, Image1, Arrow, Image2, Image3, Button, content1, HeadingContent }) => {

    return (

        <>

            <styled.Container>

                <styled.BlogHeading>
                    {BlogHead}
                </styled.BlogHeading>

            </styled.Container>

            <styled.Container>
                <styled.Cards>
                    <styled.ImageBox>
                        <styled.Image src={Image1}></styled.Image>
                        <styled.ContentBox>
                            <styled.Heading>{HeadingContent}</styled.Heading>
                            {content1}
                            <styled.ImageDiv>
                                <styled.Image src={Arrow}></styled.Image>
                            </styled.ImageDiv>
                        </styled.ContentBox>
                    </styled.ImageBox>
                    <styled.ImageBox>
                        <styled.Image src={Image2}></styled.Image>
                        <styled.ContentBox>
                            <styled.Heading>{HeadingContent}</styled.Heading>
                            {content1}
                            <styled.ImageDiv>
                                <styled.Image src={Arrow}></styled.Image>
                            </styled.ImageDiv>
                        </styled.ContentBox>
                    </styled.ImageBox>
                    <styled.ImageBox>
                        <styled.Image src={Image3}></styled.Image>
                        <styled.ContentBox>
                            <styled.Heading>{HeadingContent}</styled.Heading>
                            {content1}
                            <styled.ImageDiv>
                                <styled.Image src={Arrow}></styled.Image>
                            </styled.ImageDiv>
                        </styled.ContentBox>
                    </styled.ImageBox>
                </styled.Cards>


            </styled.Container>

            <styled.ContainerButton>
                <styled.StyledButton>{Button}</styled.StyledButton>
            </styled.ContainerButton>






        </>


    );
}

export default Blog