import React from 'react';
import { render } from '@testing-library/react';
import Header from './Header'; 
jest.mock('../styles/Header.styles', () => ({
  HeaderContainer: 'div',
  LeftSection: 'div',
  Image: 'img',
  CompanyText: 'p',
  RightSection: 'div',
  SmallSection: 'div',
  HeaderText: 'h1',
  StyledButton: 'button',
}));

describe('Header Component', () => {
  const mockProps = {
    // Define mock props here
    CompanyLogo: 'logo.jpg',
    CompanyName: 'Test Company',
    projectsText: 'Projects',
    aboutText: 'About Us',
    servicesText: 'Our Services',
    Button: 'Get Started',
  };

  test('renders Header component with provided props', () => {
    const { getByText, getByAltText } = render(<Header {...mockProps} />);

    expect(getByText('Test Company')).toBeInTheDocument();
    expect(getByText('Projects')).toBeInTheDocument();
    expect(getByText('About Us')).toBeInTheDocument();
    expect(getByText('Our Services')).toBeInTheDocument();
    expect(getByText('Get Started')).toBeInTheDocument();

    expect(getByAltText('logo.jpg')).toBeInTheDocument();
  });

});
