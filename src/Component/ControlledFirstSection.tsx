import FirstSection from "./FirstSection";
import BrandImage from "../assets/FirstSectionAssets/image 6.png";
import Logo1 from "../assets/FirstSectionAssets/Group 13.png";
import Logo2 from "../assets/FirstSectionAssets/Group 14.png";
import Logo3 from "../assets/FirstSectionAssets/Group 15.png";
import Logo4 from "../assets/FirstSectionAssets/Mask group.png";
import Logo5 from "../assets/FirstSectionAssets/Vector (1).png";



export default function FirstSectionControlled() {
    return (

        <FirstSection
            BrandImage={BrandImage}
            Button={"Learn More"}
            TextClient={"Powering next-gen companies"}
            Logo1={Logo1}
            Logo2={Logo2}
            Logo3={Logo3}
            Logo4={Logo4}
            Logo5={Logo5}
            Heading={"Building Brands in the"}
            spanText={"Digital Age"}
            paragraph={"Your partner in navigating the ever-evolving landscape of digital marketing. From conceptualization to execution, we craft tailored solutions that drive results and elevate your brand to new heights."}
        >
        </FirstSection>

    )
}
