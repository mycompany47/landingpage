import React from 'react';
import { render } from '@testing-library/react';
import Blog from './BlogSection'; 

jest.mock('../styles/Blog.styles', () => ({
  Container: 'div',
  BlogHeading: 'h1',
  Cards: 'div',
  ImageBox: 'div',
  Image: 'img',
  ContentBox: 'div',
  Heading: 'h2',
  ImageDiv: 'div',
  StyledButton: 'button',
  ContainerButton: 'div',
}));

describe('Blog Component', () => {
  const mockProps = {
    BlogHead: 'Blog Heading',
    Image1: 'image1.jpg',
    Image2: 'image2.jpg',
    Image3: 'image3.jpg',
    content1: 'Blog content',
    HeadingContent: 'Heading Content',
    Arrow: 'arrow.svg',
    Button: 'Read More',
  };

  test('renders Blog component with provided props', () => {
    const { getByText, getByAltText } = render(<Blog {...mockProps} />);

    expect(getByText('Blog Heading')).toBeInTheDocument();
    expect(getByText('Blog content')).toBeInTheDocument();
    expect(getByText('Heading Content')).toBeInTheDocument();
    expect(getByText('Read More')).toBeInTheDocument();

    expect(getByAltText('image1.jpg')).toBeInTheDocument();
    expect(getByAltText('image2.jpg')).toBeInTheDocument();
    expect(getByAltText('image3.jpg')).toBeInTheDocument();
    expect(getByAltText('arrow.svg')).toBeInTheDocument();
  });

});
