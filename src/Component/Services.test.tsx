import React from 'react';
import { render } from '@testing-library/react';
import Services from './Services';

jest.mock('../styles/Services.styles', () => ({
  Container: 'div',
  ExploreHeading: 'h1',
  ExploreParagraph: 'p',
  Cards: 'div',
  Cards1: 'div',
  Image: 'img',
  CardHead: 'h2',
  CardPara: 'p',
  StyledButton: 'button',
}));

describe('Services Component', () => {
  const mockProps = {
    exploreText: 'Explore',
    exploreparagraph: 'Explore paragraph',
    CardLogo1: 'logo1.jpg',
    CardLogo2: 'logo2.jpg',
    CardLogo3: 'logo3.jpg',
    CardLogo4: 'logo4.jpg',
    CardHead1: 'Head 1',
    CardHead2: 'Head 2',
    CardHead3: 'Head 3',
    CardHead4: 'Head 4',
    CardPara1: 'Para 1',
    CardPara2: 'Para 2',
    CardPara3: 'Para 3',
    CardPara4: 'Para 4',
    Button: 'Get Started',
  };

  test('renders Services component with provided props', () => {
    const { getByText, getByAltText } = render(<Services {...mockProps} />);

    expect(getByText('Explore')).toBeInTheDocument();
    expect(getByText('Explore paragraph')).toBeInTheDocument();

    expect(getByAltText('logo1.jpg')).toBeInTheDocument();
    expect(getByAltText('logo2.jpg')).toBeInTheDocument();
    expect(getByAltText('logo3.jpg')).toBeInTheDocument();
    expect(getByAltText('logo4.jpg')).toBeInTheDocument();

    expect(getByText('Head 1')).toBeInTheDocument();
    expect(getByText('Head 2')).toBeInTheDocument();
    expect(getByText('Head 3')).toBeInTheDocument();
    expect(getByText('Head 4')).toBeInTheDocument();

    expect(getByText('Para 1')).toBeInTheDocument();
    expect(getByText('Para 2')).toBeInTheDocument();
    expect(getByText('Para 3')).toBeInTheDocument();
    expect(getByText('Para 4')).toBeInTheDocument();

    expect(getByText('Get Started')).toBeInTheDocument();
  });

});
