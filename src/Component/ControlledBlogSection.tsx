import Blog from "./BlogSection"

import Reactangle1 from "../assets/BlogAssets/Rectangle 22.png"
import Reactangle2 from "../assets/BlogAssets/Rectangle 6.png"
import Reactangle3 from "../assets/BlogAssets/unsplash_6lcT2kRPvnI.png"
import Arrow from "../assets/BlogAssets/Group 2166.png";


export default function ControlledBlog() {
    return (
        <Blog  Button={"View all Posts"} Arrow={Arrow} HeadingContent={"Realtime analytics"} BlogHead={"Our Blogs"} Image1={Reactangle1} Image2={Reactangle2} Image3={Reactangle3} content1={"“The results have been incredible. With Power Digital, it feels like they’re in our trench, supporting and understanding us. They’re like a partner and mentor in helping us get where we want to be. “The results have been incredible."}></Blog>
    )
}
