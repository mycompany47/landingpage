import React from 'react';
import { render } from '@testing-library/react';
import TestimonialSection from './TestimonialSection'; // Update the path to your component

// Mock the styled components or use jest.mock to mock the entire module
jest.mock('../styles/Testimonial.styles', () => ({
  Container: 'div',
  TestimonialSection: 'div',
  TestimonialImage: 'div',
  Image: 'img',
  TestimonialTextBox: 'div',
  TestimonialText: 'p',
  TestimonialTextPara: 'p',
  TestimonialArrow: 'span',
}));

describe('TestimonialSection Component', () => {
  const mockProps = {
    // Define mock props here
    TestimonialText: 'Testimonial Text',
    TestimonialImage: 'testimonial.jpg',
    TestimonialPara: 'Testimonial Paragraph',
    Arrow: '→',
  };

  test('renders TestimonialSection component with provided props', () => {
    const { getByText, getByAltText } = render(<TestimonialSection {...mockProps} />);

    // Assert that elements with specific text or alt attributes are rendered
    expect(getByText('Testimonial Text')).toBeInTheDocument();
    expect(getByText('Testimonial Paragraph')).toBeInTheDocument();
    expect(getByText('→')).toBeInTheDocument();

    expect(getByAltText('testimonial.jpg')).toBeInTheDocument();
  });

  // Add more test cases for specific interactions, state changes, etc. if needed
});
