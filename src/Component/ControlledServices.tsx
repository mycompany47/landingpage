import Header from "./Header";
import Services from "./Services";

import CardLogo1 from "../assets/ServicesAsseets/24-Vector Graphic.png";
import CardLogo2 from "../assets/ServicesAsseets/Group.png";
import CardLogo3 from "../assets/ServicesAsseets/Group 57.png";
import CardLogo4 from "../assets/ServicesAsseets/Group (1).png";


export default function ControlledServices() {
    return (
        <Services exploreText={"Explore Our Services"}
        Button={"Learn More"}
        exploreparagraph={"We are self-service data analytics software that lets you create visually."} CardLogo1={CardLogo1} CardLogo2={CardLogo2} CardLogo3={CardLogo3} CardLogo4={CardLogo4} CardHead1={"App Development"} CardHead2={"Web Designing"} CardHead3={"Graphic Designing"} CardHead4={"Digital Marketing"} CardPara1={"ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod metus vel sem bibendum, a bibendum justo tempor. Sed vel lectus"} CardPara2={"ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod metus vel sem bibendum, a bibendum justo tempor. Sed vel lectus"} CardPara3={"ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod metus vel sem bibendum, a bibendum justo tempor. Sed vel lectus"} CardPara4={"ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod metus vel sem bibendum, a bibendum justo tempor. Sed vel lectus"}      ></Services>
    )
}
