import React from "react";
import * as styled from "../styles/Services.styles"


interface ServicesProps {
    children?: React.ReactNode;
    exploreText: string;
    exploreparagraph: string;
    CardLogo1: string;
    CardLogo2: string;
    CardLogo3: string;
    CardLogo4: string;
    CardHead1: string;
    CardHead2: string;
    CardHead3: string;
    CardHead4: string;
    CardPara1: string;
    CardPara2: string;
    CardPara3: string;
    CardPara4: string;
    Button: string

};
const Services: React.FC<ServicesProps> = ({ children, Button, exploreText,CardPara1,CardPara2, CardPara3, CardPara4, CardHead1, CardHead2, CardHead3, CardHead4, CardLogo1, CardLogo2, CardLogo3, CardLogo4, exploreparagraph }) => {

    return (

        <>

            <styled.Container>

                <styled.ExploreHeading>
                    {exploreText}
                </styled.ExploreHeading>

            </styled.Container>

            <styled.Container>
                <styled.ExploreParagraph>
                    {exploreparagraph}
                </styled.ExploreParagraph>
            </styled.Container>
            <styled.Container>
                <styled.Cards>
                    <styled.Cards1>
                        <styled.Image src={CardLogo1}></styled.Image>
                            <styled.CardHead>{CardHead1}</styled.CardHead>
                            <styled.CardPara>{CardPara1}</styled.CardPara>
                    </styled.Cards1>
                    <styled.Cards1>
                        <styled.Image src={CardLogo2}></styled.Image>
                        <styled.CardHead>{CardHead2}</styled.CardHead>
                        <styled.CardPara>{CardPara2}</styled.CardPara>
                    </styled.Cards1>
                    <styled.Cards1>
                        <styled.Image src={CardLogo3}></styled.Image>
                        <styled.CardHead>{CardHead3}</styled.CardHead>
                        <styled.CardPara>{CardPara3}</styled.CardPara>
                    </styled.Cards1>
                    <styled.Cards1>
                        <styled.Image src={CardLogo4}></styled.Image>
                        <styled.CardHead>{CardHead4}</styled.CardHead>
                        <styled.CardPara>{CardPara4}</styled.CardPara>
                    </styled.Cards1>
                </styled.Cards>
            </styled.Container >

            <styled.Container>
                <styled.StyledButton>{Button}</styled.StyledButton>
            </styled.Container>




        </>


    );
}

export default Services