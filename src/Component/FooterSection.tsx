import React from "react";
import * as styled from "../styles/Footer.styles";


interface FooterProps {
    children?: React.ReactNode;
    Conten1: string;
    Conten2: string;
    Conten3: string;
    Conten4: string;
    ContentHead: string;
    listItems: string[]; // Assuming listItems is an array of strings
    listItemHeading: string[];
    listItemHeadingTwo: string[];
    listItemHeadingThree: string[];
    listItemsTwo: string[];
    SocialImg: string;
    listItemsThree: string[];
};
const Footer: React.FC<FooterProps> = ({ children, SocialImg, listItemsThree, listItemsTwo, listItemHeadingThree, listItemHeadingTwo, listItemHeading, listItems, Conten1, Conten2, ContentHead, Conten3, Conten4 }) => {

    return (

        <>

            <styled.MainContainer>
                <styled.Container>

                    <styled.Card>
                        <styled.Box>
                            <styled.BoxHeading>
                                {ContentHead}
                            </styled.BoxHeading>

                            <styled.Content>
                                {Conten1}
                            </styled.Content>
                            
                            <styled.SocialBox>
                                <styled.SocialImage src={SocialImg}></styled.SocialImage>
                            </styled.SocialBox>


                        </styled.Box>


                        <styled.BoxList>
                            <styled.ListsHeading>
                                {listItemHeading.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}
                            </styled.ListsHeading>
                            <styled.Lists>
                                {listItems.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}
                            </styled.Lists>
                        </styled.BoxList>

                        <styled.BoxList>
                            <styled.ListsHeading>
                                {listItemHeadingTwo.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}
                            </styled.ListsHeading>
                            <styled.Lists>
                                {listItemsTwo.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}
                            </styled.Lists>
                        </styled.BoxList>

                        <styled.BoxList>
                            <styled.ListsHeading>
                                {listItemHeadingThree.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}
                            </styled.ListsHeading>
                            <styled.Lists>
                                {listItemsThree.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}
                            </styled.Lists>
                        </styled.BoxList>

                    </styled.Card>


                </styled.Container>
            </styled.MainContainer>








        </>


    );
}

export default Footer