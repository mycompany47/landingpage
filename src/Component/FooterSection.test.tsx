import React from 'react';
import { render } from '@testing-library/react';
import Footer from './FooterSection'; 

jest.mock('../styles/Footer.styles', () => ({
  MainContainer: 'div',
  Container: 'div',
  Card: 'div',
  Box: 'div',
  BoxHeading: 'h1',
  Content: 'p',
  SocialBox: 'div',
  SocialImage: 'img',
  BoxList: 'div',
  ListsHeading: 'h2',
  Lists: 'ul',
}));

describe('Footer Component', () => {
  const mockProps = {
    // Define mock props here
    Conten1: 'Content 1',
    Conten2: 'Content 2',
    Conten3: 'Content 3',
    Conten4: 'Content 4',
    ContentHead: 'Footer Heading',
    listItems: ['Item 1', 'Item 2', 'Item 3'],
    listItemHeading: ['Heading 1', 'Heading 2'],
    listItemHeadingTwo: ['Heading 3', 'Heading 4'],
    listItemHeadingThree: ['Heading 5', 'Heading 6'],
    listItemsTwo: ['Item 4', 'Item 5'],
    listItemsThree: ['Item 6', 'Item 7'],
    SocialImg: 'social.jpg',
  };

  test('renders Footer component with provided props', () => {
    const { getByText, getByAltText } = render(<Footer {...mockProps} />);

    // Assert that elements with specific text or alt attributes are rendered
    expect(getByText('Footer Heading')).toBeInTheDocument();
    expect(getByText('Content 1')).toBeInTheDocument();
    expect(getByText('Content 2')).toBeInTheDocument();
    expect(getByText('Content 3')).toBeInTheDocument();
    expect(getByText('Content 4')).toBeInTheDocument();
    expect(getByText('Heading 1')).toBeInTheDocument();
    expect(getByText('Heading 2')).toBeInTheDocument();
    expect(getByText('Heading 3')).toBeInTheDocument();
    expect(getByText('Heading 4')).toBeInTheDocument();
    expect(getByText('Heading 5')).toBeInTheDocument();
    expect(getByText('Heading 6')).toBeInTheDocument();

    expect(getByAltText('social.jpg')).toBeInTheDocument();
    expect(getByText('Item 1')).toBeInTheDocument();
    expect(getByText('Item 2')).toBeInTheDocument();
    expect(getByText('Item 3')).toBeInTheDocument();
    expect(getByText('Item 4')).toBeInTheDocument();
    expect(getByText('Item 5')).toBeInTheDocument();
    expect(getByText('Item 6')).toBeInTheDocument();
    expect(getByText('Item 7')).toBeInTheDocument();
  });

});
