import Header from "./Header";
import logo from "../assets/HeaderAssets/Vector.png";

export default function HeaderLists() {
    return (
        <Header CompanyLogo={logo} CompanyName={"Digital agency"} projectsText={"projects"} servicesText={"Services"} aboutText={"About"} Button={"Get in Touch"}></Header>
    )
}
